import waterflowsim.Cell;
import waterflowsim.Simulator;
import waterflowsim.Vector2D;
import waterflowsim.WaterSourceUpdater;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;

/**
 * Panel that draws WaterFlowSim.
 *
 * @author Slavomír Verner
 * @version 1.0
 */
public class MyPanel extends JPanel
{
	private static final float STROKE_RATIO = 3.0f;
	private static final float ARROW_OFFSET = 20.0f;
	private static final int ARROW_WING_HEIGHT = 10;
	private static final int ARROW_WING_WIDTH = 8;
	private static final int FONT_SIZE = 12;
	private static final int MAX_WATER_GREEN = 153;
	
	private Vector2D<Integer> dimension;
	private Cell[] cells;
	private double deltaX, deltaY;
	private double sizeMultiplier;
	private double mapEndX, mapEndY;
	private double translationX, translationY;
	
	private int rawX1, rawY1, rawX2, rawY2;
	private int selectionX1, selectionY1, selectionX2, selectionY2;
	private boolean selecting;
	
	private boolean terrainSet;
	private double minTerrainLevel, maxTerrainLevel;
	private int[] terrainRed;
	private int[] terrainGreen;
	
	/**
	 * Sets mouse listener and mouse motion listener for selecting area.
	 */
	public MyPanel()
	{
		selecting = false;
		terrainSet = false;
		
		addMouseListener(new MouseListener()
		{
			@Override
			public void mouseClicked(MouseEvent e) { }
			
			@Override
			public void mousePressed(MouseEvent e)
			{
				rawX1 = e.getX();
				rawY1 = e.getY();
				rawX2 = rawX1;
				rawY2 = rawY1;
				selecting = true;
			}
			
			@Override
			public void mouseReleased(MouseEvent e)
			{
				updateSelection(e);
				int x1Index = (int)((selectionX1 - translationX) / sizeMultiplier / deltaX);
				int y1Index = (int)((selectionY1 - translationY) / sizeMultiplier / deltaY);
				int x2Index = (int)((selectionX2 - translationX) / sizeMultiplier / deltaX);
				int y2Index = (int)((selectionY2 - translationY) / sizeMultiplier / deltaY);
				
				//user can select anything outside map, but it will be capped to map area
				int lastX = dimension.x - 1;
				int lastY = dimension.y - 1;
				Main.showGraph(Math.max(0, Math.min(lastX, x1Index)),
							   Math.max(0, Math.min(lastY, y1Index)),
							   Math.max(0, Math.min(lastX, x2Index)),
							   Math.max(0, Math.min(lastY, y2Index)));
				
				selecting = false;
				repaint();
			}
			
			@Override
			public void mouseEntered(MouseEvent e) { }
			
			@Override
			public void mouseExited(MouseEvent e) { }
		});
		
		addMouseMotionListener(new MouseMotionAdapter()
		{
			@Override
			public void mouseDragged(MouseEvent e)
			{
				if(selecting)
				{
					updateSelection(e);
					repaint();
				}
			}
		});
	}
	
	/**
	 * Updates second point of selection and sets smaller point to be selectionX/Y1
	 * and second point selectionX/Y2.
	 *
	 * @param e mouse event from parent method
	 */
	private void updateSelection(MouseEvent e)
	{
		rawX2 = e.getX();
		rawY2 = e.getY();
		//selectionX1 and selectionY1 are always
		//smaller than or equal to selectionX2 and selectionY2
		if(rawX1 < rawX2)
		{
			selectionX1 = rawX1;
			selectionX2 = rawX2;
		}
		else
		{
			selectionX1 = rawX2;
			selectionX2 = rawX1;
		}
		if(rawY1 < rawY2)
		{
			selectionY1 = rawY1;
			selectionY2 = rawY2;
		}
		else
		{
			selectionY1 = rawY2;
			selectionY2 = rawY1;
		}
	}
	
	/**
	 * Sets all necessary global variables.
	 */
	private void setGlobalVariables()
	{
		int width = getWidth();
		int height = getHeight();
		Vector2D<Double> delta = Simulator.getDelta();
		deltaX = Math.abs(delta.x);
		deltaY = Math.abs(delta.y);
		double biggerDelta = Math.max(deltaX, deltaY);
		deltaX /= biggerDelta;
		deltaY /= biggerDelta;
		
		dimension = Simulator.getDimension();
		cells = Simulator.getData();
		
		//Size multiplier of all elements by smaller dimension of window
		sizeMultiplier = Math.min(width / (dimension.x * deltaX),
								  height / (dimension.y * deltaY));
		
		double mapWidth = deltaX * dimension.x * sizeMultiplier;
		double mapHeight = deltaY * dimension.y * sizeMultiplier;
		
		//Upper left edges of map
		translationX = (width - mapWidth) / 2;
		translationY = (height - mapHeight) / 2;
		
		//Down right edges of map
		mapEndX = translationX + mapWidth;
		mapEndY = translationY + mapHeight;
	}
	
	/**
	 * Repaints whole panel.
	 *
	 * @param g graphics that will be painted on
	 */
	@Override
	public void paint(Graphics g)
	{
		super.paint(g);
		Graphics2D g2d = (Graphics2D)g;
		
		setGlobalVariables();
		
		g2d.setFont(new Font(Font.DIALOG,
							 Font.PLAIN,
							 (int)(FONT_SIZE * sizeMultiplier)));
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
							 RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setStroke(new BasicStroke((float)(STROKE_RATIO * sizeMultiplier),
									  BasicStroke.CAP_SQUARE,
									  BasicStroke.JOIN_ROUND));
		if(!terrainSet)
		{
			setTerrain();
		}
		drawWater(g2d);
		
		drawSourceNameAndDirection(g2d);
		
		if(selecting)
		{
			g2d.setColor(Color.BLACK);
			g2d.drawRect(selectionX1,
						 selectionY1,
						 selectionX2 - selectionX1,
						 selectionY2 - selectionY1);
		}
	}
	
	/**
	 * Sets terrain so that lowest point is green and highest is red.
	 */
	private void setTerrain()
	{
		terrainRed = new int[cells.length];
		terrainGreen = new int[cells.length];
		
		maxTerrainLevel = 0;
		minTerrainLevel = Integer.MAX_VALUE;
		for(Cell cell : cells)
		{
			double level = cell.getTerrainLevel();
			if(maxTerrainLevel < level)
			{
				maxTerrainLevel = level;
			}
			if(minTerrainLevel > level)
			{
				minTerrainLevel = level;
			}
		}
		
		for(int i = 0; i < cells.length; i++)
		{
			double terrainRatio = (cells[i].getTerrainLevel() - minTerrainLevel)
					/ (maxTerrainLevel - minTerrainLevel);
			terrainRatio = Math.max(0.0, terrainRatio);
			terrainRatio = Math.min(1.0, terrainRatio);
			
			if(terrainRatio > 0.5)
			{
				terrainRed[i] = 255;
				terrainGreen[i] = 255 - (int)(255 * (terrainRatio - 0.5) * 2);
			}
			else
			{
				terrainRed[i] = (int)(255 * terrainRatio * 2);
				terrainGreen[i] = 255;
			}
		}
		
		terrainSet = true;
	}
	
	/**
	 * Draws water by creating lines across every position where cell is not dry.
	 *
	 * @param g2d 2d graphics to draw on
	 */
	private void drawWater(Graphics2D g2d)
	{
		AffineTransform defaultTransform = g2d.getTransform();
		g2d.translate(translationX, translationY);
		g2d.scale(sizeMultiplier, sizeMultiplier);
		
		double[] waterLevels = new double[cells.length];
		double minWaterLevel = Integer.MAX_VALUE;
		double maxWaterLevel = 0;
		
		for(int i = 0; i < cells.length; i++)
		{
			Cell cell = cells[i];
			if(!cell.isDry())
			{
				double currentLevel = cell.getWaterLevel();
				if(currentLevel < 0)
				{
					waterLevels[i] = 0;
				}
				else
				{
					waterLevels[i] = currentLevel;
					if(maxWaterLevel < currentLevel)
					{
						maxWaterLevel = currentLevel;
					}
					if(minWaterLevel > currentLevel)
					{
						minWaterLevel = currentLevel;
					}
				}
			}
		}
		
		double maxDifference = maxWaterLevel - minWaterLevel;
		Color color;
		int arrayIndex = 0;
		
		for(int y = 0; y < dimension.y; y++)
		{
			for(int x = 0; x < dimension.x; x++)
			{
				
				if(cells[arrayIndex].isDry())
				{
					color = new Color(terrainRed[arrayIndex],
									  terrainGreen[arrayIndex],
									  0);
				}
				else
				{
					double waterRatio = (waterLevels[arrayIndex] - minWaterLevel) / maxDifference;
					waterRatio = Math.max(0.0, Math.min(1.0, waterRatio));
					
					color = new Color(0,
									  MAX_WATER_GREEN - (int)(MAX_WATER_GREEN * waterRatio),
									  255);
				}
				
				//Incrementing arrayIndex is same as y * dimension.x + x
				arrayIndex++;
				
				g2d.setColor(color);
				
				double xP = x * deltaX;
				double yP = y * deltaY;
				g2d.draw(new Line2D.Double(xP, yP, xP, yP));
			}
		}
		
		g2d.setTransform(defaultTransform);
	}
	
	/**
	 * Draws arrow that shows direction of the water source
	 * and name of the water source next to arrow.
	 *
	 * @param g2d 2d graphics to draw on
	 */
	private void drawSourceNameAndDirection(Graphics2D g2d)
	{
		g2d.setColor(Color.BLACK);
		
		for(WaterSourceUpdater source : Simulator.getWaterSources())
		{
			String s = source.getName();
			int index = source.getIndex();
			int textWidth = g2d.getFontMetrics().stringWidth(s);
			//Arrow length
			double length = textWidth + ARROW_WING_HEIGHT * sizeMultiplier;
			Vector2D<Double> gradient = cells[index].getGradient();
			
			double angle = Math.atan(Math.abs(gradient.y / gradient.x));
			boolean xPositive = gradient.x > 0;
			boolean yPositive = gradient.y > 0;
			if(xPositive)
			{
				if(!yPositive)
				{
					angle = -angle;
				}
				angle -= Math.PI;
			}
			else if(yPositive)
			{
				angle = -angle;
			}
			
			//offsetX/Y = arrow offset from source
			double unitOffsetX = -Math.cos(Math.PI / 2 - angle);
			double unitOffsetY = Math.sin(Math.PI / 2 - angle);
			double offsetX = unitOffsetX * ARROW_OFFSET;
			double offsetY = unitOffsetY * ARROW_OFFSET;
			//x/y = arrow coordinates
			double x1 = translationX + sizeMultiplier * ((index % dimension.x) * deltaX + offsetX);
			double y1 = translationY + sizeMultiplier * ((index / dimension.x) * deltaY + offsetY);
			double x2 = x1 + Math.cos(angle) * length;
			double y2 = y1 + Math.sin(angle) * length;
			
			double textHeight = g2d.getFontMetrics().getDescent() * sizeMultiplier;
			double stringOffsetX = offsetX * sizeMultiplier;
			double stringOffsetY = offsetY * sizeMultiplier;
			double stringHeightX = textHeight * unitOffsetX;
			double stringHeightY = textHeight * unitOffsetY;
			double stringWidthX = textWidth * unitOffsetY;
			double stringWidthY = - textWidth * unitOffsetX;
			//stringX/Y = text coordinates without translation/rotation
			double stringX = x1 + stringOffsetX;
			double stringY = y1 + stringOffsetY;
			//sX/Y = real text coordinates
			double sX2 = stringX + stringHeightX;
			double sY2 = stringY + stringHeightY;
			double sX1 = sX2 + stringWidthX;
			double sY1 = sY2 + stringWidthY;
			
			//If arrow or text is outside of window then they are moved on the other side of river
			if(isOffDrawArea(new double[] {x1, x2, sX1, sX2},
							 new double[] {y1, y2, sY1, sY2}))
			{
				double ox = 2 * stringOffsetX;
				double oy = 2 * stringOffsetY;
				x1 -= ox;
				x2 -= ox;
				y1 -= oy;
				y2 -= oy;
				//Text is moved by its own height towards the source
				stringX = x1 - stringOffsetX + 2 * stringHeightX;
				stringY = y1 - stringOffsetY + 2 * stringHeightY;
				
				sX2 = x1 - stringOffsetX - stringHeightX;
				sY2 = y1 - stringOffsetY - stringHeightY;
				sX1 = sX2 + stringWidthX;
				sY1 = sY2 + stringWidthY;
				
				//If arrow or text is still outside of window
				//then arrow will be moved on source position
				if(isOffDrawArea(new double[] {x1, x2, sX1, sX2},
								 new double[] {y1, y2, sY1, sY2}))
				{
					//Texts stays at other side of arrow just with offset together with arrow
					ox /= 2;
					oy /= 2;
					x1 += ox;
					x2 += ox;
					y1 += oy;
					y2 += oy;
					
					//Push arrow back to map if x1/y1 is outside of map
					double pushByX = pushXToMap(x1);
					x1 += pushByX;
					x2 += pushByX;
					
					double pushByY = pushYToMap(y1);
					y1 += pushByY;
					y2 += pushByY;
					
					//Push arrow back to map if x2/y2 is outside of map
					pushByX = pushXToMap(x2);
					x1 += pushByX;
					x2 += pushByX;
					
					pushByY = pushYToMap(y2);
					y1 += pushByY;
					y2 += pushByY;
					
					//Push arrow and text back to map if sX1/sY1 is outside of map
					sX1 = x1 + ox + stringHeightX;
					pushByX = pushXToMap(sX1);
					x1 += pushByX;
					x2 += pushByX;
					stringX = x1 + stringOffsetX + pushByX;
					
					sY1 = y1 + oy + stringHeightY;
					pushByY = pushYToMap(sY1);
					y1 += pushByY;
					y2 += pushByY;
					stringY = y1 + stringOffsetY + pushByY;
					
					//Push arrow and text back to map if sX2/sY2 is outside of map
					sX2 = x1 + ox + stringHeightX + stringWidthX;
					pushByX = pushXToMap(sX2);
					x1 += pushByX;
					x2 += pushByX;
					stringX += pushByX;
					
					sY2 = y1 + oy + stringHeightY + stringWidthY;
					pushByY = pushYToMap(sY2);
					y1 += pushByY;
					y2 += pushByY;
					stringY += pushByY;
				}
			}
			
			drawArrow(g2d, x1, y1, x2, y2);
			
			drawString(g2d, s, stringX, stringY, angle, textWidth);
		}
	}
	
	/**
	 * Returns width by witch x must be pushed back to map.
	 *
	 * @param x coordinate that will be checked
	 * @return x width by witch x must be pushed back to map (0 if it is inside map)
	 */
	private double pushXToMap(double x)
	{
		return Math.max(translationX, Math.min(mapEndX, x)) - x;
	}
	
	/**
	 * Returns height by witch y must be pushed back to map.
	 *
	 * @param y coordinate that will be checked
	 * @return y height by witch y must be pushed back to map (0 if it is inside map)
	 */
	private double pushYToMap(double y)
	{
		return Math.max(translationY, Math.min(mapEndY, y)) - y;
	}
	
	/**
	 * Returns true if any of given coordinates is outside of window.
	 *
	 * @param xArray array of x coordinates
	 * @param yArray array of y coordinates
	 * @return true if any coordinate is outside of window, false otherwise
	 */
	private boolean isOffDrawArea(double[] xArray, double[] yArray)
	{
		for(double x : xArray)
		{
			if(x < translationX || x > mapEndX)
			{
				return true;
			}
		}
		for(double y : yArray)
		{
			if(y < translationY || y > mapEndY)
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Draws text in given angle and at given position.
	 *
	 * @param g2d 2d graphics to draw on
	 * @param s text that will be drawn
	 * @param x x of text
	 * @param y y of text
	 * @param angle angle of text
	 * @param textSize string width
	 */
	private void drawString(Graphics2D g2d, String s, double x, double y, double angle, int textSize)
	{
		angle %= Math.PI * 2;
		
		AffineTransform defaultTransform = g2d.getTransform();
		g2d.translate(x, y);
		g2d.rotate(angle);
		
		//flip text if in bad angle for reading
		double finalAngle = angle < 0 ? 2 * Math.PI + angle : angle;
		if(finalAngle > Math.PI * 0.5 && finalAngle < Math.PI * 1.5)
		{
			g2d.rotate(Math.PI);
			g2d.translate(-textSize, 2 * g2d.getFontMetrics().getDescent());
		}
		
		g2d.drawString(s, 0, 0);
		g2d.setTransform(defaultTransform);
	}
	
	/**
	 * Draws arrow starting at [x1, y1] and pointing at [x2, y2].
	 *
	 * @param g2d 2d graphics to draw on
	 * @param x1 the x coordinate of the start point
	 * @param y1 the y coordinate of the start point
	 * @param x2 the x coordinate of the end point
	 * @param y2 the y coordinate of the end point
	 */
	private void drawArrow(Graphics2D g2d, double x1, double y1, double x2, double y2)
	{
		g2d.draw(new Line2D.Double(x1, y1, x2, y2));
		
		double ux = x2 - x1;
		double uy = y2 - y1;
		
		double uLength = Math.hypot(ux, uy);
		
		//unit vector
		ux /= uLength;
		uy /= uLength;
		
		//start of arrow head
		double cx = x2 - ux * ARROW_WING_HEIGHT * sizeMultiplier;
		double cy = y2 - uy * ARROW_WING_HEIGHT * sizeMultiplier;
		
		//position of arrow wings
		double d1x = cx + ARROW_WING_WIDTH * uy * sizeMultiplier;
		double d1y = cy - ARROW_WING_WIDTH * ux * sizeMultiplier;
		double d2x = cx - ARROW_WING_WIDTH * uy * sizeMultiplier;
		double d2y = cy + ARROW_WING_WIDTH * ux * sizeMultiplier;
		
		g2d.draw(new Line2D.Double(d1x, d1y, x2, y2));
		g2d.draw(new Line2D.Double(d2x, d2y, x2, y2));
	}
	
	/**
	 * Fits this panel into parent frame.
	 *
	 * @param frame parent frame
	 */
	public void fit(JFrame frame)
	{
		frame.setLocation(frame.getX() + (int)translationX,
						  frame.getY() + (int)translationY);
		frame.setSize(frame.getWidth() - (int)(translationX * 2),
					  frame.getHeight() - (int)(translationY * 2));
	}
	
	/**
	 * Returns lowest terrain level.
	 *
	 * @return lowest terrain level
	 */
	public double getMinTerrainLevel()
	{
		return minTerrainLevel;
	}
	
	/**
	 * Returns highest terrain level.
	 *
	 * @return highest terrain level
	 */
	public double getMaxTerrainLevel()
	{
		return maxTerrainLevel;
	}
	
	/**
	 * Returns name of this panel.
	 *
	 * @return name of this panel
	 */
	@Override
	public String toString()
	{
		return "Simulation panel";
	}
}
