import waterflowsim.Cell;
import waterflowsim.Simulator;
import java.util.LinkedHashMap;

/**
 * Saves water levels and reduces them after hitting limit.
 *
 * @author Slavomír Verner
 * @version 1.0
 */
public class WaterData
{
	private static final int MAX_ELEMENTS = 100;
	
	private int collectAt;
	private int index;
	private double lastStep;
	//first key is time, second is index of getData array and value is water level of given cell
	private LinkedHashMap<Double, LinkedHashMap<Integer, Double>> waterLevelsAtTime;
	
	/**
	 * Initializes variables
	 */
	public WaterData()
	{
		collectAt = 1;
		index = 0;
		lastStep = 0.0;
		waterLevelsAtTime = new LinkedHashMap<>();
	}
	
	/**
	 * Collects water levels from current simulation state.
	 *
	 * @param step size of current simulation step
	 */
	public void collectData(double step)
	{
		if(++index == collectAt)
		{
			index = 0;
			LinkedHashMap<Integer, Double> waterLevels = new LinkedHashMap<>();
			Cell[] data = Simulator.getData();
			for(int i = 0; i < data.length; i++)
			{
				Cell cell = data[i];
				if(!cell.isDry())
				{
					waterLevels.put(i, cell.getWaterLevel());
				}
			}
			waterLevelsAtTime.put((lastStep += step), waterLevels);
			
			if(waterLevelsAtTime.size() >= MAX_ELEMENTS)
			{
				removeCloseMeasurements();
			}
		}
	}
	
	/**
	 * Removes close measurements and increments point of collecting data.
	 */
	private void removeCloseMeasurements()
	{
		LinkedHashMap<Double, LinkedHashMap<Integer, Double>> temp = new LinkedHashMap<>();
		Double[] times = waterLevelsAtTime.keySet().toArray(new Double[0]);
		double averageDistance = times[times.length - 1] / (waterLevelsAtTime.size() / 2.0);
		
		temp.put(times[0], waterLevelsAtTime.get(times[0]));
		int j = 0;
		for(int i = 1; i < times.length; i++)
		{
			if(times[i] - times[j] > averageDistance)
			{
				temp.put(times[i], waterLevelsAtTime.get(times[i]));
				j = i;
			}
		}
		
		waterLevelsAtTime = temp;
		collectAt++;
	}
	
	/**
	 * Returns copy of water levels at time.
	 *
	 * @return copy of water levels at time.
	 */
	public LinkedHashMap<Double, LinkedHashMap<Integer, Double>> getDataCopy()
	{
		//return copy, so map is not concurrently accessed
		return new LinkedHashMap<>(waterLevelsAtTime);
	}
}
