import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;

/**
 * Extends ChartPanel and only overrides toString
 *
 * @author Slavomír Verner
 * @version 1.0
 */
public class MyChartPanel extends ChartPanel
{
	/**
	 * Constructor passes chart to super
	 *
	 * @param chart chart that will be painted on panel
	 */
	public MyChartPanel(JFreeChart chart)
	{
		super(chart);
	}
	
	/**
	 * Returns name of this panel.
	 *
	 * @return name of this panel
	 */
	@Override
	public String toString()
	{
		return "Graph";
	}
}
