import javax.swing.JPanel;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Task for timer.
 *
 * @author Slavomír Verner
 * @version 1.0
 */
public class Task extends TimerTask
{
	private final Timer timer;
	private final JPanel panel;
	private final boolean[] running; //must be reference
	
	/**
	 * Constructor only passes parameters.
	 *
	 * @param timer timer to which will be scheduled the task
	 * @param panel panel that will be repainted
	 * @param running singleton boolean array that indicates whether simulation is running or not
	 */
	public Task(Timer timer, JPanel panel, boolean[] running)
	{
		this.timer = timer;
		this.panel = panel;
		this.running = running;
	}
	
	/**
	 * Work of this task.
	 */
	@Override
	public void run()
	{
		if(running[0])
		{
			Main.taskWork(panel);
		}
		timer.schedule(new Task(timer, panel, running),
					   Main.getTimerPeriod());
	}
}