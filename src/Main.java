import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.graphics2d.svg.SVGGraphics2D;
import waterflowsim.Simulator;
import waterflowsim.Vector2D;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.print.*;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.Timer;

/**
 * Launches GUI and simulation.
 *
 * @author Slavomír Verner
 * @version 1.0
 */
public class Main
{
	private static final String PROGRAM_TITLE = "A17B0387P";
	private static final int LEGEND_ROWS = 11;
	private static final int DEFAULT_SCENARIO = 0;
	private static final int INITIAL_WIDTH = 850;
	private static final int INITIAL_HEIGHT = 850;
	private static final int DEFAULT_SPEED = 1; //default simulation step times 1000
	private static final int DEFAULT_TIMER_PERIOD = 100; //ms
	
	private static final JFreeChart CHART = makeChart();
	private static final JFrame CHART_FRAME = new JFrame();
	private static final WaterData WATER_DATA = new WaterData();
	
	private static Double simulationStep;
	private static int timerPeriod;
	private static int x1, y1, x2, y2; //selected window for graph
	
	/**
	 * Sets scenario and GUI and timer that runs simulator.
	 *
	 * @param args first and only argument is number of scenario
	 */
	public static void main(String[] args)
	{
		int scenario = DEFAULT_SCENARIO;
		if(args.length > 0)
		{
			try
			{
				scenario = Integer.parseInt(args[0]);
			}
			catch(NumberFormatException e)
			{
				System.out.println("Wrong scenario parameter");
			}
			if(scenario < 0 || scenario > Simulator.getScenarios().length - 1)
			{
				System.out.println("No such scenario");
				scenario = DEFAULT_SCENARIO;
			}
		}
		else
		{
			System.out.println("No scenario parameter");
		}
		System.out.println("Running scenario number " + scenario);
		Simulator.runScenario(scenario);
		
		simulationStep = DEFAULT_SPEED / 1000.0;
		timerPeriod = DEFAULT_TIMER_PERIOD;
		Locale.setDefault(Locale.US);
		
		boolean[] running = {false}; //boolean must be reference for action listener
		
		MyChartPanel myChartPanel = new MyChartPanel(CHART);
		CHART_FRAME.setLayout(new BorderLayout());
		CHART_FRAME.add(myChartPanel, BorderLayout.CENTER);
		CHART_FRAME.setTitle("Chart");
		CHART_FRAME.pack();
		CHART_FRAME.setLocationRelativeTo(null);
		
		MyPanel myPanel = new MyPanel();
		myPanel.setDoubleBuffered(true);
		myPanel.setPreferredSize(new Dimension(INITIAL_WIDTH, INITIAL_HEIGHT));
		
		JFrame frame = new JFrame(PROGRAM_TITLE);
		frame.setLayout(new BorderLayout());
		frame.add(getControls(frame,
							  myPanel,
							  myChartPanel,
							  running),
				  BorderLayout.SOUTH);
		frame.add(myPanel);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		Timer simulationTimer = new Timer();
		Task task = new Task(simulationTimer, myPanel, running);
		simulationTimer.schedule(task, 0);
	}
	
	/**
	 * This method represents work that is done by Task instance.
	 *
	 * @param panel simulation panel
	 */
	public static void taskWork(JPanel panel)
	{
		double step = simulationStep; //step is updated at the beginning of cycle
		Simulator.nextStep(step);
		WATER_DATA.collectData(step);
		if(CHART_FRAME.isVisible())
		{
			updateDataset();
		}
		panel.repaint();
	}
	
	/**
	 * Updates dataset and replace current dataset with new one.
	 * New dataset must be created instead of concurrently changing the old one.
	 */
	private static void updateDataset()
	{
		CHART.getXYPlot().setDataset(getDataset());
	}
	
	/**
	 * Sets area of selection of water levels, updates dataset and shows graph to user.
	 *
	 * @param x1 x point of beginning of selection
	 * @param y1 y point of beginning of selection
	 * @param x2 x point of end of selection
	 * @param y2 y point of end of selection
	 */
	public static void showGraph(int x1, int y1, int x2, int y2)
	{
		Main.x1 = x1;
		Main.y1 = y1;
		Main.x2 = x2;
		Main.y2 = y2;
		updateDataset();
		CHART_FRAME.setVisible(true);
	}
	
	/**
	 * Creates chart with empty dataset.
	 *
	 * @return chart with empty dataset
	 */
	private static JFreeChart makeChart()
	{
		return ChartFactory.createXYLineChart(
				"Average water level in time",
				"Simulation step",
				"Water level in meters",
				new XYSeriesCollection());
	}
	
	/**
	 * Creates dataset for chart from average water levels of selected area.
	 *
	 * @return dataset for chart
	 */
	private static XYSeriesCollection getDataset()
	{
		XYSeries series = new XYSeries(String.format(
				"From [%d,%d] to [%d,%d]", x1, y1, x2, y2));
		WATER_DATA.getDataCopy().forEach((time, data) ->
		{
			int cellsWithWater = 0;
			double average = 0;
			for(int y = y1; y <= y2; y++)
			{
				int yOffset = y * Simulator.getDimension().x;
				int lastX = yOffset + x2;
				for(int x = yOffset + x1; x <= lastX; x++)
				{
					Double value = data.get(x);
					if(value != null) //null = dry
					{
						cellsWithWater++;
						average += value;
					}
				}
			}
			average /= cellsWithWater;
			series.add(time.doubleValue(), average);
		});
		XYSeriesCollection newDataset = new XYSeriesCollection();
		newDataset.addSeries(series);
		return newDataset;
	}
	
	/**
	 * Returns timer period.
	 *
	 * @return timer period
	 */
	public static int getTimerPeriod()
	{
		return timerPeriod;
	}
	
	/**
	 * Creates and returns screenshot of panel.
	 *
	 * @param panel panel for screenshot
	 * @param width width of image
	 * @param height height of image
	 * @return screenshot of panel
	 */
	private static BufferedImage screenshot(JPanel panel, int width, int height)
	{
		BufferedImage screenshot = new BufferedImage(
				panel.getWidth(),
				panel.getHeight(),
				BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = screenshot.createGraphics();
		panel.printAll(g2);
		Image temp = screenshot.getScaledInstance(width,
												  height,
												  Image.SCALE_SMOOTH);
		BufferedImage image = new BufferedImage(width,
												height,
												BufferedImage.TYPE_INT_RGB);
		g2 = image.createGraphics();
		g2.drawImage(temp, 0, 0, null);
		return image;
	}
	
	/**
	 * Exports image of panel.
	 *
	 * @param panel panel for screenshot and export
	 * @param width width of image
	 * @param height height of image
	 */
	private static void exportImage(JPanel panel, int width, int height)
	{
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle("Save as");
		fileChooser.setAcceptAllFileFilterUsed(false);
		
		String[] descriptions =
		{
			"PNG (*.png)",
			"JPG (*.jpg)",
			"BMP (*.bmp)",
		};
		String[] extensions =
		{
			"png",
			"jpg",
			"bmp",
		};
		
		for(int i = 0; i < extensions.length; i++)
		{
			fileChooser.addChoosableFileFilter(new FileNameExtensionFilter(
					descriptions[i], extensions[i]));
		}
		
		if(panel instanceof MyPanel)
		{
			fileChooser.addChoosableFileFilter(new FileNameExtensionFilter(
					"SVG (*.svg)", "svg"));
		}
		
		if(fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION)
		{
			String format = ((FileNameExtensionFilter)
					fileChooser.getFileFilter()).getExtensions()[0];
			if(format.equals("svg"))
			{
				SVGGraphics2D g2 = new SVGGraphics2D(panel.getWidth(),
													 panel.getHeight());
				panel.paint(g2);
				
				try
				{
					PrintWriter writer = new PrintWriter(
							fileChooser.getSelectedFile() + ".svg",
							StandardCharsets.UTF_8);
					writer.println(g2.getSVGElement());
					writer.close();
				}
				catch(IOException e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				BufferedImage image = screenshot(panel, width, height);
				File file = new File(fileChooser.getSelectedFile() + "." + format);
				try
				{
					ImageIO.write(image, format, file);
				}
				catch(IOException e)
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Sends image of panel to printer.
	 *
	 * @param panel panel that will be printed
	 * @param width size of image
	 * @param height height of image
	 */
	private static void print(JPanel panel, int width, int height)
	{
		if(panel != null)
		{
			PrinterJob job = PrinterJob.getPrinterJob();
			if(job.printDialog())
			{
				job.setPrintable((graphics, pageFormat, pageIndex) ->
				{
					if(pageIndex != 0)
					{
						return Printable.NO_SUCH_PAGE;
					}
					
					int imageWidth = width;
					int imageHeight = height;
					double pageWidth = pageFormat.getImageableWidth();
					double pageHeight = pageFormat.getImageableHeight();
					
					if(imageWidth > pageWidth)
					{
						imageHeight *= pageWidth / imageWidth;
						imageWidth = (int)pageWidth;
					}
					if(imageHeight > pageHeight)
					{
						imageWidth *= pageHeight / imageHeight;
						imageHeight = (int)pageHeight;
					}
					
					BufferedImage image = screenshot(panel,
													 panel.getWidth(),
													 panel.getHeight());
					
					graphics.translate((int)pageFormat.getImageableX(),
									   (int)pageFormat.getImageableY());
					
					graphics.drawImage(image,
									   0,
									   0,
									   imageWidth,
									   imageHeight,
									   null);
					
					return Printable.PAGE_EXISTS;
				});
				
				try
				{
					job.print();
				}
				catch(PrinterAbortException a)
				{
					System.out.println("Print aborted");
				}
				catch(Exception p)
				{
					p.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Sets controls of simulation and returns it as JPanel.
	 *
	 * @param frame parent frame
	 * @param myPanel panel with simulation
	 * @param myChartPanel panel with chart
	 * @param running singleton boolean array that indicates whether simulation is running or not
	 * @return simulation controls
	 */
	private static JPanel getControls(JFrame frame,
									  MyPanel myPanel,
									  MyChartPanel myChartPanel,
									  boolean[] running)
	{
		JButton pauseButton = new JButton("Start");
		pauseButton.setToolTipText("Pause/Continue simulation");
		pauseButton.addActionListener(e ->
		{
			running[0] = !running[0];
			pauseButton.setText(running[0] ? "Pause" : "Continue");
		});
		
		JSlider stepSlider = new JSlider(1, 100, DEFAULT_SPEED);
		stepSlider.setToolTipText("How big simulation step is");
		Hashtable<Integer, JLabel> stepSliderLabels = new Hashtable<>();
		stepSliderLabels.put(1, new JLabel("0.001"));
		stepSliderLabels.put(50, new JLabel("Step length"));
		stepSliderLabels.put(100, new JLabel("0.100"));
		stepSlider.setLabelTable(stepSliderLabels);
		stepSlider.setPaintLabels(true);
		stepSlider.addChangeListener(e -> simulationStep = stepSlider.getValue() / 1000.0);
		
		JSlider delaySlider = new JSlider(1, 1000, DEFAULT_TIMER_PERIOD);
		delaySlider.setToolTipText("How often is simulation updated (lower is faster)");
		Hashtable<Integer, JLabel> delaySliderLabels = new Hashtable<>();
		delaySliderLabels.put(1, new JLabel("1"));
		delaySliderLabels.put(500, new JLabel("Refresh period"));
		delaySliderLabels.put(1000, new JLabel("1000"));
		delaySlider.setLabelTable(delaySliderLabels);
		delaySlider.setPaintLabels(true);
		delaySlider.addChangeListener(e -> timerPeriod = delaySlider.getValue());
		
		JMenuItem chartMenuItem = new JMenuItem("Graph");
		chartMenuItem.setToolTipText("Creates graph of average water levels of whole map");
		chartMenuItem.addActionListener(e ->
		{
			Vector2D<Integer> dimension = Simulator.getDimension();
			showGraph(0, 0, dimension.x - 1, dimension.y - 1);
		});
		
		JMenuItem fitMenuItem = new JMenuItem("Fit in");
		fitMenuItem.setToolTipText("Removes extra space to fit map into panel");
		fitMenuItem.addActionListener(e -> myPanel.fit(frame));
		
		JLabel widthLabel = new JLabel("Width");
		widthLabel.setHorizontalAlignment(SwingConstants.CENTER);
		JSpinner widthSpinner = new JSpinner(new SpinnerNumberModel(
				INITIAL_WIDTH, 10, 10000, 1));
		widthSpinner.setToolTipText("Width of exported/printed panel");
		
		JLabel heightLabel = new JLabel("Height");
		heightLabel.setHorizontalAlignment(SwingConstants.CENTER);
		JSpinner heightSpinner = new JSpinner(new SpinnerNumberModel(
				INITIAL_HEIGHT, 10, 10000, 1));
		heightSpinner.setToolTipText("Height of exported/printed panel");
		
		JComboBox<JPanel> exportComboBox = new JComboBox<>(
				new JPanel[] {myPanel, myChartPanel});
		((JLabel)exportComboBox.getRenderer()).setHorizontalAlignment(SwingConstants.CENTER);
		exportComboBox.setToolTipText("Select panel for exporting or printing");
		
		JButton imageExportButton = new JButton("Export");
		imageExportButton.setToolTipText("Exports image of selected panel");
		imageExportButton.addActionListener(e ->
		{
			JPanel panel = (JPanel)exportComboBox.getSelectedItem();
			if(panel != null)
			{
				exportImage(panel,
							(int)widthSpinner.getValue(),
							(int)heightSpinner.getValue());
			}
		});
		
		JButton sizeButton = new JButton("Current size");
		sizeButton.setToolTipText("Sets export size to current panel size");
		sizeButton.addActionListener(e ->
		{
			JPanel panel = (JPanel)exportComboBox.getSelectedItem();
			if(panel != null)
			{
				widthSpinner.setValue(panel.getWidth());
				heightSpinner.setValue(panel.getHeight());
			}
		});
		
		JButton printButton = new JButton("Print");
		printButton.setToolTipText("Sends image of selected panel to printer");
		printButton.addActionListener(e -> print((JPanel)exportComboBox.getSelectedItem(),
												 (int)widthSpinner.getValue(),
												 (int)heightSpinner.getValue()));
		
		
		JFrame legendFrame= new JFrame("Legend");
		JLabel[] labels = new JLabel[LEGEND_ROWS];
		legendFrame.setLayout(new GridLayout(LEGEND_ROWS, 2));
		int halfRows = LEGEND_ROWS / 2;
		for(int i = 0; i < LEGEND_ROWS; i++)
		{
			int red = 255, green = 255;
			if(i < halfRows)
			{
				red = 255 * i / halfRows;
			}
			else if(i >= LEGEND_ROWS - halfRows)
			{
				green = 255 * (LEGEND_ROWS - i - 1) / halfRows;
			}
			legendFrame.add(new JPanel()).setBackground(new Color(red, green, 0));
			
			labels[i] = new JLabel();
			((JLabel)legendFrame.add(labels[i])).setHorizontalAlignment(SwingConstants.CENTER);
			//labels must be updated later because simulation is not yet ready
		}
		
		legendFrame.setLocationRelativeTo(null);
		
		JMenuItem legendMenuItem = new JMenuItem("Legend");
		legendMenuItem.addActionListener(e ->
		{
			double minLevel = myPanel.getMinTerrainLevel();
			double maxLevel = myPanel.getMaxTerrainLevel();
			double difference = maxLevel - minLevel;
			int r = LEGEND_ROWS - 1;
			for(int i = 0; i < LEGEND_ROWS; i++)
			{
				labels[i].setText(String.format("%.2f m", minLevel + difference * i / r));
			}
			legendFrame.pack();
			legendFrame.setVisible(true);
		});
		
		JFrame exportFrame = new JFrame("Export");
		exportFrame.setLayout(new GridLayout(0,2));
		exportFrame.add(exportComboBox);
		exportFrame.add(sizeButton);
		exportFrame.add(widthLabel);
		exportFrame.add(widthSpinner);
		exportFrame.add(heightLabel);
		exportFrame.add(heightSpinner);
		exportFrame.add(imageExportButton);
		exportFrame.add(printButton);
		exportFrame.pack();
		exportFrame.setLocationRelativeTo(null);
		
		JMenuItem exportMenuItem = new JMenuItem("Export");
		exportMenuItem.setToolTipText("Export or print");
		exportMenuItem.addActionListener(e -> exportFrame.setVisible(true));
		
		JMenu menu = new JMenu("Options");
		menu.add(chartMenuItem);
		menu.add(legendMenuItem);
		menu.add(fitMenuItem);
		menu.add(exportMenuItem);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.add(menu);
		menuBar.setBackground(pauseButton.getBackground());
		
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());
		panel.add(pauseButton);
		panel.add(stepSlider);
		panel.add(delaySlider);
		panel.add(menuBar);
		
		return panel;
	}
}
